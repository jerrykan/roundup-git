issues.roundup-tracker.org:

 * log into issues.roundup-tracker.org
 * get a working copy of roundup/website/issues from the SCM, either via
   hg clone http://hg.code.sf.net/p/roundup/code
   or download a snapshot:
   http://sourceforge.net/p/roundup/code/ci/default/tarball

 * check the differences
   diff -ur /home/YOURUSERID/roundup/website/issues/ /home/roundup/trackers/roundup/
 * copy the files into the tracker instance, using sudo:
      sudo -u roundup cp <file> /home/roundup/trackers/roundup/...
   or use rsync to check and only copy the changed files as user roundup like
      rsync -rvc /home/YOURUSERID/roundup/website/issues/ trackers/roundup/
      HINT: old files will not be deleted by this rsync command 
 * restart the roundup server:
      sudo /etc/init.d/roundup restart

[1] All services hosted on sf.net:
 * log into sf.net (Generic sf instructions can be found here:
   http://sourceforge.net/apps/trac/sourceforge/wiki/Shell%20service )
      ssh -t <user>,roundup@shell.sourceforge.net create
 * set project_home:
      project_home=/home/project-web/roundup
      cd ${project_home}
 * read up on other people changes and add yours
      vim ${project_home}/logbuch.txt
 * update the working copy of the SCM roundup source (includes www and wiki)
      cd ${project_home}/src/roundup
      hg pull -u
   (The warning about "Not trusting file /home/hg/p/roundup/code/.hg/hgrc
   from untrusted user" can be ignored.)
 * When done working in the shell, you can destroy it early to free resources:
      shutdown

www.roundup-tracker.org:
 * follow [1].
 * activate the virtualenv
      . ${project_home}/docbuilder/bin/activate
 * go to the now current source directory
      cd ${project_home}/src/roundup/website/www
 * (build requirement: sphinx and the sphinxcontrib-cheeseshop plugin
      http://pypi.python.org/pypi/sphinxcontrib-cheeseshop)
 * build it
      make html
 * you may also "make clean"
 * install it
     cp -r ./html/* ${project_home}/htdocs/
   or alternatively (leaving out the --dry-run later)
     rsync --dry-run -v --checksum --recursive ./html/* ${project_home}/htdocs/

(I think I can simplify the Makefile above such that the installation will be included as a make target.)

wiki.roundup-tracker.org:
 * follow [1].
 * the main wiki configuration is here
      vim persistent/wiki/wikiconfig.py
 * go to the now current source directory
      cd ${project_home}/src/roundup/website/wiki
 * copy the files into the right places:
      cp static/roundup/* ${project_home}/htdocs/_wiki/
      cp wiki/data/plugin/theme/roundup.py ${project_home}/persistent/wiki/data/plugin/theme/
